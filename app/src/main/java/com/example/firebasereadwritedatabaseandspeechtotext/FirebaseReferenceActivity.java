package com.example.firebasereadwritedatabaseandspeechtotext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class FirebaseReferenceActivity extends AppCompatActivity {
    private DatabaseReference databaseReference;
    private TextView tvFbData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_reference);
        tvFbData = (TextView)findViewById(R.id.tvFbData);

        FirebaseApp.initializeApp(this);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("User");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getChildrenCount() > 0) {
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if (map.get("user1") != null) {
                        String user = map.get("user1").toString();
                        tvFbData.setText(user);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void addOrUpdateDB(View view){
        DatabaseReference addOrUpdateDB = FirebaseDatabase.getInstance().getReference().child("User");
        HashMap map = new HashMap();
        map.put("user1", "sample user");
        addOrUpdateDB.updateChildren(map);
    }
}
