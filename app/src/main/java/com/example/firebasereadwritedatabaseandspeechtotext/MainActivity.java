package com.example.firebasereadwritedatabaseandspeechtotext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void openActivity1(View view){
        Intent intent = new Intent(MainActivity.this, SpeechToTextActivity.class);
        startActivity(intent);
        finish();
    }
    public void openActivity2(View view){
        Intent intent = new Intent(MainActivity.this, FirebaseReferenceActivity.class);
        startActivity(intent);
        finish();
    }
}
